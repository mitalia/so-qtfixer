from stackapi import StackAPI
import cPickle
import re

SITE = StackAPI('stackoverflow')
cache = {}

try:
    with open("cache.pickle", "rb") as f:
        cache = cPickle.load(f)
except IOError as ex:
    pass

def cfetch(*args, **kwargs):
    k = repr((args, kwargs))
    if k not in cache:
        cache[k] = SITE.fetch(*args, **kwargs)
    return cache[k]

posts = []
url_queries = [
        'http*://qt.nokia.com/doc/*',
        'http*://*qt-project.org/doc/*',
        'http*://doc.trolltech.com/*',
        'http*://doc.qt.digia.com/*',
        ]
for u in url_queries:
    posts+=cfetch('search/excerpts', url=u)['items']
res = [
        re.compile(r'http[s]?://qt\.nokia\.com/doc/[^\s]*[\w]', re.I),
        re.compile(r'http[s]?://(?:www.)?qt-project\.org/doc/[^\s]*[\w]', re.I),
        re.compile(r'http[s]?://doc\.trolltech\.com/[^\s]*[\w]', re.I),
        re.compile(r'http[s]?://doc\.qt.digia\.com/[^\s]*[\w]', re.I),
        ]

for post in posts:
    for r in res:
        for m in r.findall(post['body']):
            print m

with open("cache.pickle", "wb") as f:
    cPickle.dump(cache, f)
